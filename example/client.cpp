#include <iostream>
#include "httplib.h"
#define SERVER_IP "121.4.56.74"
#define SERVER_PORT 8888
int main()
{
    httplib::Client client(SERVER_IP, SERVER_PORT); // 实例化client客户端，用于搭建客户端
    httplib::MultipartFormData item;
    item.name = "file";
    item.filename = "hello.txt";
    item.content = "Hello World!";
    item.content_type = "text/plain";

    httplib::MultipartFormDataItems items;
    items.push_back(item);

    auto res = client.Post("/multipart", items);
    std::cout << res->status << std::endl;
    //std::cout<<res->content_provider_success_<<std::endl;

    std::cout << res->body << std::endl;

    return 0;
}
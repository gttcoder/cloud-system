#include "httplib.h"
void Hello(const httplib::Request &req, httplib::Response &resp)
{
    resp.set_content("Hello world!", "text/plain");
    resp.status = 200;
}
void Numbders(const httplib::Request &req, httplib::Response &resp)
{
    auto num = req.matches[1]; // 0里面保存的是整体path,往后下标中保存的捕捉的数据
    resp.set_content(num, "text/plain");
    resp.status = 200;
}
void Multipart(const httplib::Request &req, httplib::Response &resp)
{
    auto ret = req.has_file("file");
    if (ret==false)
    {
        std::cout << "not file upload\n";
        resp.status = 400;
        return;
    }
    const auto &file = req.get_file_value("file");
    resp.body.clear();
    resp.body = file.filename; // 文件名称
    resp.body += "\n";
    resp.body += file.content; // 文件内容
    resp.set_header("Content-Type", "text/plain");
    resp.status = 200;
    return;
}
int main()
{
    httplib::Server server;   // 实例化以server类的对象用于搭建服务器
    // server.Get("/hi", Hello); // 注册一个针对/hi的Get请求的处理函数映射关系
    // server.Get(R"(/numbers/(\d+))", Numbders);
    server.Post("/multipart",Multipart);
    server.listen("0.0.0.0",8888);

    return 0;

//     server:server.cpp 
// 	g++ -o $@ $^  -std=c++11 -lpthread
// .PHONY:clean
// clean:
// 	rm -rf server
}
#include <iostream>
#include <string>
#include <vector>
int split(const std::string &str, const std::string &sep, std::vector<std::string> *array)
{
    int count = 0;
    size_t pos = 0, index = 0;
    while (1)
    {
        pos = str.find(sep, index);
        if (pos == std::string::npos)
        {
            break;
        }
        if (pos == index)
        {
            index = pos + sep.size();
            continue;
        }
        std::string temp = str.substr(index, pos - index);
        array->push_back(temp);
        count++;
        index = pos + sep.size();
    }
    if (index < str.size() - 1)
    {
        std::string temp = str.substr(index);
        array->push_back(temp);
    }
    return count;
}

int main()
{
    std::string str = "abc cde  vfr";
    std::vector<std::string> array;
    split(str, " ", &array);
    for (auto &a : array)
    {
        std::cout << a << std::endl;
    }

    return 0;
}
#ifndef __MY_CLOUD__
#define __MY_CLOUD__
#include"Data.hpp"
#include<Windows.h>

namespace gttCloud
{
	class BackUp
	{
	private:
		std::string _back_dir;
		DataManager* _data;
	public:
		BackUp(const std::string& back_dir, const std::string& backfile)
			:_back_dir(back_dir)
		{
			_data = new DataManager(backfile);
		}
		std::string GetFileIdentifier(const std::string& filename)
		{
			//a.txt-fsize-mtime;
			std::stringstream ss;
			FileUtil fu(filename);
			ss << fu.FileName() << "-" << fu.FileSize() << "-" << fu.LastMTime();
			return ss.str();
		}
		bool RunModule()
		{
			while (1)
			{
				FileUtil fu(_back_dir);
				std::vector<std::string> array;
				fu.ScanDiretory(&array);
				for (auto& a : array)
				{
					std::string id = GetFileIdentifier(a);
					_data->Insert(a, id);
				}
				Sleep(1);
				//_sleep(1);
			}
			
		}
	};
}


#endif

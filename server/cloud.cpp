#include "util.hpp"
#include "config.hpp"
#include "data.hpp"
#include "hot.hpp"
#include"service.hpp"
#include<thread>
void FileUtilTest(const std::string &filename)
{
  // gttCloud::FileUtil fu(filename);
  // std::cout<<"文件大小："<<fu.FileSize()<<std::endl;
  // std::cout<<"文件最近访问时间："<<fu.LastATime()<<std::endl;
  // std::cout<<"文件最近修改时间"<<fu.LastMTime()<<std::endl;
  // std::cout<<"文件名称："<<fu.FileName()<<std::endl;

  // gttCloud::FileUtil fu(filename);
  // std::string body;
  // fu.GetContent(&body);

  // gttCloud::FileUtil nfu("./hello.txt");
  // nfu.SetContent(body);
  // return ;
  // std::string packname=filename+".lz";
  // gttCloud::FileUtil fu(filename);
  // fu.Compress(packname);

  // gttCloud::FileUtil pfu(packname);
  // pfu.UnCompress("./hello.txt");
  gttCloud::FileUtil fu(filename);
  fu.CreateDirectory();
  std::vector<std::string> array;

  if (fu.ScanDiretory(&array))
  {
    for (auto &a : array)
    {
      std::cout << a << std::endl;
    }
  }

  return;
}
void DataTest(std::string filename)
{
  gttCloud::DataManager data;
  std::vector<gttCloud::BackupInfo> array;
  data.GetAll(&array);

  for (auto &a : array)
  {
    std::cout << a.pack_flag << std::endl;
    std::cout << a.fsize << std::endl;
    std::cout << a.mtime << std::endl;
    std::cout << a.atime << std::endl;
    std::cout << a.real_path << std::endl;
    std::cout << a.pack_path << std::endl;
    std::cout << a.url << std::endl;
  }

  // gttCloud::BackupInfo info;

  // gttCloud::DataManager data;
  // std::cout << "-------------------Insert--------------" << std::endl;
  // info.NewBackupInfo(filename);
  // data.Insert(info);

  // gttCloud::BackupInfo tmp;
  // data.GetOneByURL("/download/bundle.h", &tmp);

  // std::cout << tmp.pack_flag << std::endl;
  // std::cout << tmp.fsize << std::endl;
  // std::cout << tmp.mtime << std::endl;
  // std::cout << tmp.atime << std::endl;
  // std::cout << tmp.real_path << std::endl;
  // std::cout << tmp.pack_path << std::endl;
  // std::cout << tmp.url << std::endl;
  // std::cout << "-------------------update and getall--------------" << std::endl;

  // info.pack_flag = true;
  // data.Update(info);
  // std::vector<gttCloud::BackupInfo> array;
  // data.GetAll(&array);
  // for (auto &a : array)
  // {
  //   std::cout << a.pack_flag << std::endl;
  //   std::cout << a.fsize << std::endl;
  //   std::cout << a.mtime << std::endl;
  //   std::cout << a.atime << std::endl;
  //   std::cout << a.real_path << std::endl;
  //   std::cout << a.pack_path << std::endl;
  //   std::cout << a.url << std::endl;
  // }

  // std::cout << "-------------------realpath--------------" << std::endl;
  // data.GetOneByRealPath(filename, &tmp);
  // std::cout << tmp.pack_flag << std::endl;
  // std::cout << tmp.fsize << std::endl;
  // std::cout << tmp.mtime << std::endl;
  // std::cout << tmp.atime << std::endl;
  // std::cout << tmp.real_path << std::endl;
  // std::cout << tmp.pack_path << std::endl;
  // std::cout << tmp.url << std::endl;
}

void JsonUtilTest()
{
  const char *name = "小明";
  int age = 19;
  float score[] = {85, 92, 89.5};
  Json::Value root;
  root["姓名"] = name;
  root["年龄"] = age;
  root["成绩"].append(score[0]);
  root["成绩"].append(score[1]);
  root["成绩"].append(score[2]);
  std::string json_str;
  gttCloud::FileUtil::JsonUtil::Serialize(root, &json_str);
  std::cout << json_str << std::endl;

  Json::Value val;
  gttCloud::FileUtil::JsonUtil::UnSerialisze(json_str, &val);
  std::cout << val["姓名"].asString() << std::endl;
  std::cout << val["年龄"].asInt() << std::endl;
  for (int i = 0; i < val["成绩"].size(); ++i)
  {
    std::cout << val["成绩"][i].asFloat() << std::endl;
  }
}
void ConfigTest()
{
  gttCloud::Config *config = gttCloud::Config::getInstance();
  std::cout << config->GetHotTime() << std::endl;
  std::cout << config->GetServerPort() << std::endl;
  std::cout << config->GetServerIP() << std::endl;
  std::cout << config->GetDownloadPrefix() << std::endl;
  std::cout << config->GetPackFileSuffix() << std::endl;
  std::cout << config->GetPackDir() << std::endl;
  std::cout << config->GetBackDir() << std::endl;
  std::cout << config->GetBackupFile() << std::endl;
}
gttCloud::DataManager *_data;
void HotTest()
{
  _data = new gttCloud::DataManager();
  gttCloud::HotManager hot;
  hot.RunModule();
}
void ServiceTest()
{
  gttCloud::Service srv;
  

  srv.RunModule();
}
int main(int argc, char *argv[])
{
  _data=new gttCloud::DataManager();
  // FileUtilTest(argv[1]);
  // JsonUtilTest();
  // ConfigTest();
  //DataTest(argv[1]);
  //HotTest();
  //ServiceTest();
  std::thread thread_hot_manager(HotTest);
  std::thread thread_service(ServiceTest);

  thread_hot_manager.join();
  thread_service.join();

  return 0;
}
